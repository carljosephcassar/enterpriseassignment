﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    public class UserBloggerDBContext : DbContext
    {
        public UserBloggerDBContext() : base("name = DefaultConnection")
        {
        }
        public DbSet<UserBlogger> UserBloggers { get; set; }
        public DbSet<Blog> Blogs { get; set; }
    }
}