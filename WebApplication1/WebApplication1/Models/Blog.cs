﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    public class Blog
    {
        [Key]
        public int ID { get; set; }
        [MaxLength(50)]
        [Required]
        public string Heading { get; set; }
        [Required]
        public string Category { get; set; }
        [Required]
        public string Tags { get; set; }
        public string Author { get; set; }
        public string ImagePath { get; set; }
        [Required]
        public string Content { get; set; }
    }
}