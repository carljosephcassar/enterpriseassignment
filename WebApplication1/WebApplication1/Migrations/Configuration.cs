namespace WebApplication1.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<WebApplication1.Models.UserBloggerDBContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(WebApplication1.Models.UserBloggerDBContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            context.UserBloggers.AddOrUpdate(p => p.ID,
               new Models.UserBlogger { ID = 126697, Name = "Carl", Surname = "Cassar", Description = "21 years" , Email = "carl@melita.com",
                                        Username = "cassar123", Password = "Sliemautd5!"}
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
               );
            //
        }
    }
}
