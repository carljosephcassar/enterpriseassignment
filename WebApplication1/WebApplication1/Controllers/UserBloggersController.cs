﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class UserBloggersController : Controller
    {
        private UserBloggerDBContext db = new UserBloggerDBContext();

        // GET: UserBloggers
      //  [Authorize]
        public ActionResult Index()
        {
            return View(db.UserBloggers.ToList());
        }

        // GET: UserBloggers/Details/5
      //  [Authorize]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserBlogger userBlogger = db.UserBloggers.Find(id);
            if (userBlogger == null)
            {
                return HttpNotFound();
            }
            return View(userBlogger);
        }

        // GET: UserBloggers/Create
       // [Authorize]
        public ActionResult Create()
        {
            return View();
        }

        // POST: UserBloggers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Blog b, UserBlogger u)
        //([Bind(Include = "ID,Name,Surname,Description,Email,Username,Password")] UserBlogger userBlogger)
        {
            //if (ModelState.IsValid)
            //{
            //    db.UserBloggers.Add(userBlogger);
            //    db.SaveChanges();
            //    return RedirectToAction("Index");
            //}
            UserBlogger ub = new UserBlogger();
            u.Name = ub.Name;
            u.Surname = ub.Surname;
            u.Description = ub.Description;
            u.Email = ub.Email;
            u.Username = ub.Username;
            u.Password = ub.Password;
            db.UserBloggers.Add(u);
            db.SaveChanges();
            ViewData["message"] = "Added Successfully";

            return RedirectToAction("Index");
        }
        // GET: UserBloggers/Edit/5
       // [Authorize]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserBlogger userBlogger = db.UserBloggers.Find(id);
            if (userBlogger == null)
            {
                return HttpNotFound();
            }
            return View(userBlogger);
        }

        // POST: UserBloggers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Name,Surname,Description,Email,Username,Password")] UserBlogger userBlogger)
        {
            if (ModelState.IsValid)
            {
                db.Entry(userBlogger).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(userBlogger);
        }

        // GET: UserBloggers/Delete/5
       // [Authorize]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserBlogger userBlogger = db.UserBloggers.Find(id);
            if (userBlogger == null)
            {
                return HttpNotFound();
            }
            return View(userBlogger);
        }

        // POST: UserBloggers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            UserBlogger userBlogger = db.UserBloggers.Find(id);
            db.UserBloggers.Remove(userBlogger);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
