﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Models;
//using static webapplication1.Memorypostedfile;

namespace webapplication1.controllers
{
    public class BlogsController : Controller
    {
        private UserBloggerDBContext db = new UserBloggerDBContext();

        // get: blogs
        //[authorize]
        public ActionResult Index()
        {
            return View(db.Blogs.ToList());
        }

        // get: blogs/details/5
        //[authorize]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Blog blog = db.Blogs.Find(id);
            if (blog == null)
            {
                return HttpNotFound();
            }
            return View(blog);
        }

        // get: blogs/create
        //[authorize]
        public ActionResult create()
        {
            return View();
        }

        // post: blogs/create
        // to protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?linkid=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]

        public ActionResult create(Blog b, HttpPostedFileBase imagepath, UserBlogger u)
        {
            Blog bl = new Blog();
            var imagename = Path.GetFileName(imagepath.FileName);

            var imgsrc = Path.Combine(Server.MapPath("\\images\\"), imagename);

            string filepathtosave = "\\images\\" + imagename;

            imagepath.SaveAs(imgsrc);

            b.ImagePath = filepathtosave;
            b.Heading = bl.Heading;
            b.Content = bl.Content;
            b.Category = bl.Category;
            b.Tags = bl.Tags;
            b.Author = System.Web.HttpContext.Current.User.Identity.Name.ToString();
            db.Blogs.Add(b);
            db.SaveChanges();
            ViewData["message"] = "Added Successfully";

            return RedirectToAction("Index");
        }

        // get: blogs/edit/5
        //[authorize]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Blog blog = db.Blogs.Find(id);
            if (blog == null)
            {
                return HttpNotFound();
            }
            return View(blog);
        }

        // post: blogs/edit/5
        // to protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?linkid=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult edit([Bind(Include = "id,heading,category,tags,author,imagepath,content")] Blog blog)
        {
            if (ModelState.IsValid)
            {
                db.Entry(blog).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(blog);
        }

        // get: blogs/delete/5
        //[authorize]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Blog blog = db.Blogs.Find(id);
            if (blog == null)
            {
                return HttpNotFound();
            }
            return View(blog);
        }

        // post: blogs/delete/5
        [HttpPost, ActionName("delete")]
        [ValidateAntiForgeryToken]
        public ActionResult deleteconfirmed(int id)
        {
            Blog blog = db.Blogs.Find(id);
            db.Blogs.Remove(blog);
            db.SaveChanges();
            return RedirectToAction("index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
